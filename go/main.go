package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"net/http"
	"os"
	"strconv"
	"strings"
	"sync"
)

const (
	// RedditLimit ex: MAX 100
	RedditLimit int = 100
	// RedditType ex: json
	RedditType string = "json"
)

var (
	avalableExtension = []string{"jpg", "jpeg", "png"}
	// SubReddit ex: "r/Animewallpaper"
	SubReddit = "r/Animewallpaper"
	baseURL   = "https://www.reddit.com/"
	// RedditTime ex: hour, day, week, month, year, all
	RedditTime     = "month"
	limit          = 250
	fileDownlaodWG = sync.WaitGroup{}
	fileCheckWG    = sync.WaitGroup{}
	// threshold for the brightness of the image (min: 0, max: 255)
	threshold = ^uint16(0) / 2
)

type (
	redditChildren struct {
		Kind string                 `json:"kind"`
		Data map[string]interface{} `json:"data"`
	}

	redditData struct {
		Dist     int              `json:"dist"`
		After    string           `json:"after"`
		Children []redditChildren `json:"children"`
	}

	redditResponse struct {
		Kind string     `json:"kind"`
		Data redditData `json:"data"`
	}
)

func main() {
	flag.IntVar(&limit, "limit", limit, "Number of posts to go through")
	flag.StringVar(&RedditTime, "time", RedditTime, "ex: hour, day, week, month, year, all")
	flag.StringVar(&SubReddit, "subreddit", SubReddit, "ex: \"r/Animewallpaper\"")
	// flag.IntVar(&threshold, "threshold", 128, "threshold for the brightness of the image (min: 0, max: 255)")
	flag.Parse()

	fmt.Printf(`[Info] Checking on the subreddit: %s
[Info] Number of posts: %d
[Info] For time range of: %s
`, SubReddit, limit, RedditTime)

	if err := os.MkdirAll("./dark-image", 0777); err != nil {
		fmt.Println("[Info] Error Creating Dir")
		os.Exit(1)
	}
	if err := os.MkdirAll("./tmp", 0777); err != nil {
		fmt.Println("[Info] Error Creating Dir")
		os.Exit(1)
	}

	var lastTotal = 0
	var after = ""
	var count = 0
	var numOfLoops = int(math.Ceil(float64(limit) / float64(RedditLimit)))

	for i := 0; i < numOfLoops; i++ {
		var qs []string
		var limitQ = "100"

		if i == (numOfLoops - 1) {
			limitQ = strconv.Itoa(limit - (RedditLimit * (numOfLoops - 1)))
		} else {
			limitQ = strconv.Itoa(RedditLimit)
		}

		qs = append(qs, "limit="+limitQ)
		qs = append(qs, "t="+RedditTime)

		if after != "" {
			qs = append(qs, "after="+after)
		}

		var url = baseURL + SubReddit + "/top." + RedditType + "?" + strings.Join(qs, "&")

		client := &http.Client{}
		req, _ := http.NewRequest("GET", url, nil)
		req.Header.Add("User-agent", "Potato 0.1")
		res, err := client.Do(req)

		if err != nil {
			panic(err)
		}
		defer res.Body.Close()
		body, err := ioutil.ReadAll(res.Body)
		var dat redditResponse
		if err := json.Unmarshal(body, &dat); err != nil {
			panic(err)
		}
		lastTotal = dat.Data.Dist
		after = dat.Data.After
		fileDownlaodWG.Add(len(dat.Data.Children))
		for _, child := range dat.Data.Children {
			fmt.Printf("[Info] Downloading image %s\n", child.Data["title"])
			count++
			fmt.Printf("[Info] No of images: %d\n", count)
			var imageURL = fmt.Sprint(child.Data["url"])
			var extension = imageURL[strings.LastIndex(imageURL, ".")+1:]

			if !contains(avalableExtension, extension) {
				fileDownlaodWG.Add(-1)
				continue
			}

			go saveImageToFolder(imageURL, strconv.Itoa(count))
		}

		if i != 0 && lastTotal < RedditLimit {
			break
		}
	}
	fileDownlaodWG.Wait()

	files, err := ioutil.ReadDir("./tmp/")
	if err != nil {
		log.Fatal(err)
	}
	fileCheckWG.Add(len(files))
	for _, f := range files {
		go checkImageFile("./tmp/" + f.Name())
	}
	fileCheckWG.Wait()
}
