package main

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"strings"
)

func saveImageToFolder(url string, filename string) {
	response, e := http.Get(url)
	if e != nil {
		log.Fatal(e)
	}
	defer response.Body.Close()

	var extension = url[strings.LastIndex(url, ".")+1:]

	var fullFilePath = "./tmp/" + filename + "." + extension

	//open a file for writing
	file, err := os.Create(fullFilePath)
	if err != nil {
		log.Print(err)
		fileDownlaodWG.Done()
		return
	}
	defer file.Close()

	_, err = io.Copy(file, response.Body)
	if err != nil {
		log.Print(err)
		fileDownlaodWG.Done()
		return
	}
	fmt.Println("[Info] Download completed.", fullFilePath)
	fileDownlaodWG.Done()
}

func copyImageToFolder(srcFilePath string, outFilePath string) {
	fmt.Printf("[Info] Moving file from %s to %s\n", srcFilePath, outFilePath)
	srcFile, err := os.Open(srcFilePath)
	check(err)
	defer srcFile.Close()

	destFile, err := os.Create(outFilePath)
	check(err)
	defer destFile.Close()

	_, err = io.Copy(destFile, srcFile)
	check(err)

	err = destFile.Sync()
	check(err)
}

func check(err error) {
	if err != nil {
		fmt.Printf("Error : %s\n", err.Error())
		os.Exit(1)
	}
}
