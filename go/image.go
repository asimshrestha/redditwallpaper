package main

import (
	"fmt"
	jpeg "image/jpeg"
	png "image/png"
	"log"
	"math"
	"os"
	"path/filepath"
	"strings"
)

func checkImageFile(imageFilePath string) {
	var fileExtension = imageFilePath[strings.LastIndex(imageFilePath, ".")+1:]
	var brightness = ^uint16(0)
	reader, err := os.Open(imageFilePath)
	if err != nil {
		log.Fatal(err)
	}
	defer reader.Close()

	switch fileExtension {
	case "png":
		fmt.Println("[Info] Decoding PNG Image")
		fmt.Println("[Info] imageFilePath: ", imageFilePath)
		brightness = checkPNGImage(reader)
		break
	case "jpg":
	case "jpeg":
		fmt.Println("[Info] Decoding JPEG | JPG Image")
		fmt.Println("[Info] imageFilePath:\t", imageFilePath)
		brightness = checkJPGImage(reader)
		break
	default:
		fileCheckWG.Add(-1)
		return
	}
	fmt.Println("[Info] Brightness:", brightness)

	if brightness <= threshold {
		_, file := filepath.Split(imageFilePath)
		fmt.Println("[Info] Pass/Fail: PASS")
		copyImageToFolder(imageFilePath, "./dark-image/"+file)
	} else {
		fmt.Println("[Info] Pass/Fail: FAIL")
	}
	fileCheckWG.Done()
}

func checkPNGImage(reader *os.File) uint16 {
	var avg = 0
	var colorSum = 0
	m, err := png.Decode(reader)
	if err != nil {
		// log.Fatal(err)
		return uint16(0)
	}
	bounds := m.Bounds()
	for y := bounds.Min.Y; y < bounds.Max.Y; y++ {
		for x := bounds.Min.X; x < bounds.Max.X; x++ {
			r, g, b, _ := m.At(x, y).RGBA()
			avg = int(math.Floor((float64(r) + float64(g) + float64(b)) / 3))
			colorSum += avg
		}
	}
	return uint16(math.Floor(float64(colorSum) / (float64(bounds.Max.Y) * float64(bounds.Max.X))))
}

func checkJPGImage(reader *os.File) uint16 {
	var avg = 0
	var colorSum = 0
	m, err := jpeg.Decode(reader)
	if err != nil {
		// log.Fatal(err)
		return uint16(0)
	}
	bounds := m.Bounds()
	for y := bounds.Min.Y; y < bounds.Max.Y; y++ {
		for x := bounds.Min.X; x < bounds.Max.X; x++ {
			r, g, b, _ := m.At(x, y).RGBA()
			avg = int(math.Floor((float64(r) + float64(g) + float64(b)) / 3))
			colorSum += avg
		}
	}
	return uint16(math.Floor(float64(colorSum) / (float64(bounds.Max.Y) * float64(bounds.Max.X))))
}
