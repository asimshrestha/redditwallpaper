
const SUB_REDDIT = 'r/Animewallpaper'; // 'r/Animewallpaper'
const URL = 'https://www.reddit.com/' + SUB_REDDIT;
const REDDIT_LIMIT = 100; // MAX 100
const REDDIT_TIME = 'month'; // hour, day, week, month, year, all
const LIMIT = 1000;
const TYPE = 'json';

const requestPromise = require('request-promise');
const fs = require('fs');
const { PNG } = require('pngjs');
const jpeg = require('jpeg-js');

const threshold = Math.floor(255/2);

(async () => {
    let dir = './images/';
    let darkDir = './dark-images/';

    if (!fs.existsSync(dir)){
        fs.mkdirSync(dir);
    }

    if (!fs.existsSync(darkDir)){
        fs.mkdirSync(darkDir);
    }

    await getImagesFromReddit();

    console.log(`\n[Info] Moving Files from "${dir}" to "${darkDir}".`);
    moveFolderAndCheckImages(dir, darkDir);
})();

function moveFolderAndCheckImages(dir, darkDir) {
    let fileInFolder = fs.readdirSync(dir, { withFileTypes: true });
    for (let i = 0; i < fileInFolder.length; i++) {
        const file = fileInFolder[i];
        let extension = file.slice(file.lastIndexOf('.') + 1);
        let brightness;
        try {
            switch (extension) {
                case 'png':
                    console.log(`[Info] Decoding png image`);
                    brightness = getBrightnessForPNG(dir, file);
                    console.log('[Info] Brightness: ', brightness);
                    break;
                case 'jpeg':
                case 'jpg':
                    console.log(`[Info] Decoding jpeg|jpg image`);
                    brightness = getBrightnessForJPG(dir, file);
                    console.log('[Info] Brightness: ', brightness);
                    break;
            }
            if (brightness <= threshold) {
                console.log(`\n[Info] Moving file to folder ${darkDir + file}`);
                fs.copyFileSync(dir + file, darkDir + file);
                console.log(`[Info] Moved file\n`);
            }
        } catch (e) {
            console.log(`[Error] Error Moving or Checking image ${file}\n`);
        }
    }
}

function getBrightnessForJPG(dir, file) {
    let colorSum = 0;
    let data = fs.readFileSync(dir + file);
    let jpg = jpeg.decode(data);
    let r, g, b, avg;
    for (let y = 0; y < jpg.height; y++) {
        for (let x = 0; x < jpg.width; x++) {
            const idx = (jpg.width * y + x) << 2;
            // Color
            r = jpg.data[idx];
            g = jpg.data[idx + 1];
            b = jpg.data[idx + 2];
            // a: png.data[idx+3]
            avg = Math.floor((r + g + b) / 3);
            colorSum += avg;
        }
    }
    let brightness = Math.floor(colorSum / (jpg.width * jpg.height));
    return brightness;
}

function getBrightnessForPNG(dir, file) {
    let colorSum = 0;
    let data = fs.readFileSync(dir + file);
    let png = PNG.sync.read(data);
    let r, g, b, avg;
    for (let y = 0; y < png.height; y++) {
        for (let x = 0; x < png.width; x++) {
            const idx = (png.width * y + x) << 2;
            // Color
            r = png.data[idx];
            g = png.data[idx + 1];
            b = png.data[idx + 2];
            // a: png.data[idx+3]
            avg = Math.floor((r + g + b) / 3);
            colorSum += avg;
        }
    }
    let brightness = Math.floor(colorSum / (png.width * png.height));
    return brightness;
}

async function getImagesFromReddit() {
    let lastTotal = 0;
    let after = '';
    let count = 0;
    let numOfLoops = Math.ceil(LIMIT/REDDIT_LIMIT);
    
    for (let i = 0; i < numOfLoops; i++) {
        let opt = {
            uri: `${URL}/top.${TYPE}`,
            method: 'GET',
            qs: {
                limit: i === (numOfLoops - 1) 
                    ? LIMIT - (REDDIT_LIMIT * (numOfLoops - 1))
                    : REDDIT_LIMIT,
                t: REDDIT_TIME
            },
            json: true
        }

        if(after !== ''){ opt.qs['after'] = after; }

        let response = await requestPromise(opt);

        if (!response) { break; }

        lastTotal = response.data.dist;
        after = response.data.after;
        
        for (let i = 0; i < response.data.children.length; i++) {
            const post = response.data.children[i].data;
            // let imageReg = /(((http:\/\/)|((https:\/\/)))[-a-zA-Z0-9@:%_\+.~#?&//=]+)\.(jpg|jpeg|gif|png|bmp|tiff|tga|svg)/g
            let imageReg = /(((http:\/\/)|((https:\/\/)))[-a-zA-Z0-9@:%_\+.~#?&//=]+)\.(jpg|jpeg|png)/g;
            if (post.url.match(imageReg)) {
                console.log(`[Info] Downloading image ${post.title}`);
                let extension = post.url.slice(post.url.lastIndexOf('.') + 1);
                let date = new Date().getTime();
                try {
                    let image = await requestPromise({ uri: post.url, encoding: null });
                    let filename = `./images/image-${count}-${date}.${extension}`;
                    fs.writeFileSync(filename, image);
                    console.log(`[Info] Done downloading image ${post.title} -> ${filename}`);
                }
                catch (e) {
                    console.log(`[Err] Error downloading image ${post.title}, error: ${e}`);
                }
            }
            count++;
            console.log(`[Info] No of images: ${count}`);
        }

        if(i !== 0 && lastTotal < REDDIT_LIMIT){ break; }
    }
}
