# Reddit Wallpaper

A cli tool to separate dark images from a sub-reddit

## How to use

Use to get all the arguments for the tool

`$ ./redditwallpaper -h` 

## Build

Requirements:
* Golang

`$ ./build.sh` 