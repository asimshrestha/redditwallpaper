#!/bin/bash
GOARCH=amd64 GOOS=linux go build -o ./build/Release/redditwallpaper-linux-amd64 ./go
GOARCH=amd64 GOOS=windows go build -o ./build/Release/redditwallpaper-windows-amd64.exe ./go
GOARCH=amd64 GOOS=darwin go build -o ./build/Release/redditwallpaper-darwin-amd64 ./go
